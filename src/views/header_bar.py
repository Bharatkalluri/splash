from gi.repository import Gtk, GdkPixbuf, Gio
from ..services.wallpaper_service import WallPaperService
from ..services.unsplash_service import UnSplashService, UnSplashImage

import threading

@Gtk.Template(resource_path='/in/bharatkalluri/splash/ui/header_bar.ui')
class HeaderBar(Gtk.HeaderBar):
    __gtype_name__ = 'header_bar'

    shuffle_button: Gtk.Button = Gtk.Template.Child()
    loading_spinner: Gtk.Spinner = Gtk.Template.Child()
    open_menu_button: Gtk.MenuButton = Gtk.Template.Child()
    save_wallpaper_button: Gtk.Button = Gtk.Template.Child()

    unsplash_service: UnSplashService = UnSplashService()
    wallpaper_service: WallPaperService = WallPaperService.get_instance()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_menu_items()
        self.async_shuffle_image()
        self.shuffle_button.connect("clicked", self.shuffle_button_on_clicked)
        self.save_wallpaper_button.connect("clicked", self.save_wallpaper_button_on_clicked)

    def shuffle_image(self):
        self.loading_spinner.start()
        random_wallpaper_metadata: UnSplashImage = self.unsplash_service.get_random_photo_metadata()
        wallpaper_file_uri = self.wallpaper_service.set_wallpaper_from_unsplash_image(random_wallpaper_metadata)
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            wallpaper_file_uri,
            width=1280,
            height=720,
            preserve_aspect_ratio=True,
        )
        from .window import SplashWindow
        SplashWindow.get_instance().wallpaper_container.set_from_pixbuf(pixbuf)
        self.set_subtitle(f"Photograph by {random_wallpaper_metadata.photographer_name} from Unsplash.com")
        self.loading_spinner.stop()

    def async_shuffle_image(self):
        thread = threading.Thread(target=self.shuffle_image, daemon=True)
        thread.start()

    def shuffle_button_on_clicked(self, button: Gtk.Button):
        self.async_shuffle_image()

    def save_wallpaper_button_on_clicked(self, button: Gtk.Button):
        self.wallpaper_service.save_current_wallpaper_in_pictures()


    def set_menu_items(self):
        menu: Gio.Menu = Gio.Menu()
        menu.append_item(Gio.MenuItem.new(_("About"), "app.about"))
        self.open_menu_button.set_menu_model(menu)

    instance = None
    @staticmethod
    def get_instance(**kwargs):
        if HeaderBar.instance is None:
            HeaderBar.instance = HeaderBar(**kwargs)
        return HeaderBar.instance
