import requests

class UnSplashImage:
    def __init__(self, json_response):
        self.image_id = json_response['id']

        urls = json_response['urls']
        self.full_url = urls['full']

        user_info = json_response.get('user')
        self.photographer_name = user_info['name']
        self.user_name = user_info['username']

        user_links = user_info['links']
        self.user_link_html = user_links['html']



class UnSplashService:
    access_key = "71847d4728771faa4852ccafc9e690590df27a4ee2fe875765e61209f18a9f6c"

    def get_random_photo_metadata(self) -> UnSplashImage:
        unsplash_response = requests.get(
            f"https://api.unsplash.com/photos/random",
            params={
                "client_id": self.access_key,
                "orientation": "landscape"
            }
        )
        response_json = unsplash_response.json()
        unsplash_image_metadata = UnSplashImage(response_json)
        return unsplash_image_metadata

